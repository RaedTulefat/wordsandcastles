class AddColumnToGames < ActiveRecord::Migration[5.1]
  def change
    add_column :games, :score, :integer, :default => 0
  end
end
