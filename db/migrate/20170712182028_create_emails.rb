class CreateEmails < ActiveRecord::Migration[4.2]
  def change
    create_table :emails do |t|
      t.string :sender
      t.string :subject
      t.text :body

      t.timestamps null: false
    end
  end
end
