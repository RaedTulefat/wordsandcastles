require 'test_helper'

class UserTest < ActiveSupport::TestCase

  # setup area
  def setup
    # create instance variable to use in the test
    @user = User.new(name: "Example User",
                     email: "user@example.com",
                     password: "password",
                     password_confirmation: "password")
  end

  # test user is valid
  test "when user created it should be valid" do
    # assert makes sure the next argument is true
    # we want to test that user is valid
    # the arugment is @user.valid? is the truth that we want to test
    # its the truth we want to assert
    # add assert infront of it
    # eg. this will pass assert true
    # assert true
    assert @user.valid?
  end

  # test name presence (not present is invalid)
  test "when user created name should be present" do
    # make name empty for this test only
    @user.name = "  " #.black? is true
    # assert not makes sure that the second argument is false
    assert_not @user.valid?
  end

  # test email presence (not present is invalid)
  test "when user created email should be present" do
    @user.email = "  "
    assert_not @user.valid?
  end

  # tests that name is not longer than 50
  test "when user created name should NOT be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  # tests that password is not longer than 50
  test "when user created password should NOT be too short" do
    @user.password = "a"
    @user.password_confirmation = "a"
    assert_not @user.valid?
  end

  # tests that email is not longer than 255
  test "when user created email should Not be too longer" do
    @user.email = "#{"a" * 244}@example.com"
    assert_not @user.valid?
  end

  # test that email address with these patterns are good
  test "when user created should accept valid email addresses" do
    # %w creates an array of elements (its a shortcut in ruby)
    valid_addresses = %w[user@example.com
                          user@example.COM
                          us-er@gmail.com
                          first.last@bob.cn
                          alice_bob@example.jp]

    # eterate throught the array
    # replace user email with each
    # make sure all are valid
    valid_addresses.each do |email|
      @user.email = email
      assert @user.valid?, "#{email.inspect} should be valid"
      # the coma message displace this as first like so i know which exactly
      # failed the test
    end
  end

  test "when user created should NOT accept valid email addresses" do
    valid_addresses = %w[userexample.com
                          user@example,COM
                          us-er@gmail-com
                          @bob.cn
                          alice_bob
                          user@exa+mple.COM
                          us-er@gma_il.com
                          first.last@bob.]
    valid_addresses.each do |email|
      @user.email = email
      assert_not @user.valid?, "#{email.inspect} should invalid"
    end
  end

  # tests email uniques is working
  test "when user created should have unique email address" do
    # .dup creates a copy instead of refering to the object
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase

    @user.save

    assert_not duplicate_user.valid?
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end


end
