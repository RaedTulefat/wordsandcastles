require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # to generate integration test
  # rails generate integration_test test_name

  # to run the intigration test
  # bundle exec rake test:integration

  test "layout links" do
    # requesting this path
    get root_path
    # should give this view
    assert_template 'pages/main'

    # not logged in
    # header
    assert_select "a[href=?]", root_path, count: 1 #2 links back to root exists
    # assert_select "a[href=?]", login_path #login link
    #
    # #footer
    # assert_select "a[href=?]", how_we_work_path, count: 2 # assets it does not exist
    # assert_select "a[href=?]", about_path #link to about exists
    assert_select "a[href=?]", contact_path #link to contact exists
    # assert_select "a[href=?]", terms_path #link to contact exists
    # assert_select "a[href=?]", privacy_path #link to contact exists
    #
    # # those links should not appear
    # assert_select "a[href=?]", logout_path(1), count: 0
    # assert_select "a[href=?]", edit_user_path(1), count: 0
  end
end
