require 'test_helper'

class PagesControllerTest < ActionController::TestCase

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | project_name"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | project_name"
  end

  test "should get how_we_work" do
    get :how_we_work
    assert_response :success
    assert_select "title", "How We Work | project_name"
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "Help | project_name"
  end

  test "should get privacy" do
    get :privacy
    assert_response :success
    assert_select "title", "Privacy | project_name"
  end

end
