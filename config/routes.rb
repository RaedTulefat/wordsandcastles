Rails.application.routes.draw do

  root                                  'pages#root'

  get     'main'                         =>  'pages#main'

  get    'start'                         =>  'games#start'
  get    'gameover'                         =>  'games#gameover'
  get    'game'                         =>  'games#game'
  get    'skip'                         =>  'games#skip'
  post   'word_check'     =>  'games#word_check'

  # receiving emails
  post  'post'    =>  'emails#post'
  get   'success' =>  'emails#success'

  get 'set_cookie' =>  'cookie#set_cookie'
  get 'get_cookie' =>  'cookie#getter'
  get 'cookie/index'
  get 'cookie/setter'


  root                          'pages#game'



  get                           'newsletters/create'
  post                          'newsletters/create'
  get     'newsletter'      =>  'newsletters#new'
  get     'signup'          =>  'users#new'
  get     'login'           =>  'sessions#new'
  post    'login'           =>  'sessions#create'
  delete  'logout'          =>  'sessions#destroy'
  get     'resend'          =>  'password_resends#new'
  post    'resend'          =>  'password_resends#create'
  get     'about'           =>  'pages#about'
  get     'help'            =>  'pages#help'
  get     'contact'         =>  'pages#contact'
  get     'landing_page'         =>  'pages#landing_page'

  get     'privacy'         =>  'pages#privacy'
  get     'terms'           =>  'pages#terms'
  get     'invite'          =>  'invites#new'
  get     'auth/:provider/callback', to: 'sessions#social_create'
  # get     'auth/facebook', to: 'sessions#social_create'

  # resources :merchants, only: [:edit, :update]
  resources :resumes
  resources :projects
  resource :newsletters
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :password_resends,    only: [:new, :create]
  resource :invites
end
