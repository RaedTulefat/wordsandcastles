OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, 'google code here', 'another google code here', {client_options: {ssl: {ca_file: Rails.root.join("cacert.pem").to_s}}}

  provider :facebook, 'facebook code here', 'facebook key here', {:client_options => {:ssl => {:ca_file => Rails.root.join("cacert.pem").to_s}}}
end
