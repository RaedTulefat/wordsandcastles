class Email < ActiveRecord::Base
  validates :sender,  presence: true
end
