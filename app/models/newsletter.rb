class Newsletter < ActiveRecord::Base
  validates :email, presence: true
  before_save :downcase_email
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 255},
                    format: { with: VALID_EMAIL_REGEX},
                    uniqueness: {case_sensitive: false}


  def downcase_email
    self.email = email.downcase
  end
end
