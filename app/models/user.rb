class User < ActiveRecord::Base
  # attr_accessor :name, :email etc etc is no longer needed because rails nows
    # these attributes exists as columns in the database
    # rails talks to the active record to get them


  # has_many :projects, dependent: :destroy
  has_many :games
  require 'securerandom'

  attr_accessor :remember_token, :activation_token, :reset_token
  # before_save   :downcase_email
  before_create :create_activation_digest
  after_create  :actions_after_successful_signup
  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 255},
                    format: { with: VALID_EMAIL_REGEX},
                    uniqueness: {case_sensitive: false}
  has_secure_password
  validates :password, length: { minimum: 8 }, allow_blank: true

  #Returns the hash Digest of the given string
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  #Returns a random token
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  #Remembers a user in the database for use in persistent sessions
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # forgets a user
  def forget
    update_attribute(:remember_digest, nil)
  end

  #Returns true if the given token matches the digest
  def authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # this was written to facture account_activation_controller line:10
  def activate
    self.update_attribute(:activated, true)
    self.update_attribute(:activated_at, Time.zone.now)
  end

  # sends activation email
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

# Sets the password reset attributes
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # sends password reset email
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def resend_activation_link
    self.activation_token  = User.new_token
    self.activation_digest = User.digest(activation_token)
    update_attribute(:activation_digest, User.digest(activation_token))
    self.send_activation_email
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.email = auth.info.email
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.password = SecureRandom.urlsafe_base64
      user.activated = true
      params = { "name" => user.name, "email" => user.email}
      begin
        user.save!
      rescue => error
        user.update(params)
      end
    end
  end

  def actions_after_successful_signup
    if self.newsletter == true
      newsletter = Mailchimp::API.new("mailchimp-api-key-here")
      newsletter.lists.subscribe("mailchimp-list-id-here",
                       { "email" => self.email})
    end
  end

  private

  # Converts email to all lower-case.
  def downcase_email
    self.email = email.downcase
  end

  def create_activation_digest
    self.activation_token  = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
end
