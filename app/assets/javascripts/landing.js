var velocity = 0.5;

function update(){
    var pos = $(window).scrollTop();
    $('.landing-page-intro').each(function() {
        var $element = $(this);
        $(this).css('backgroundPosition', '50% ' + Math.round((0 - pos) * velocity) + 'px');
    });
    $('.landing-page-intro-mob').each(function() {
        var $element = $(this);
        $(this).css('backgroundPosition', '50% ' + Math.round((0 - pos) * velocity) + 'px');
    });
};

$(window).bind('scroll', update);
