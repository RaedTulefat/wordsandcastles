// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

// Nav bar dissapearing when scrolling down
(function ($) {
  $(document).on('ready page:load', function () {

  $(document).on('ready page:load', function () {
    $(document).ready(function() {
        $("#down-arrow").click(function(event){
            // $('html, body').animate({scrollTop: '+=1050px'}, 800);
            $('html, body').animate({scrollTop: $(document).height()*0.42}, 800);
        });
    });
  });

  // $(function () {
	// 	$(window).scroll(function () {
  //           // set distance user needs to scroll before we fadeIn navbar
	// 		if ($(this).scrollTop() > 100) {
  //       $('.editors-choice').fadeIn();
	// 		} else {
	// 			$('.editors-choice:hidden').fadeOut();
	// 		}
	// 	});
	// });
  //
	// $(function () {
	// 	$(window).scroll(function () {
  //           // set distance user needs to scroll before we fadeIn navbar
	// 		if ($(this).scrollTop() > 100) {
	// 			$('#navbar').fadeOut();
	// 		} else {
	// 			$('#navbar').fadeIn();
	// 		}
	// 	});
	// });

// incase we use a down arrow in the landing page
  $(document).ready(function() {
      $("#down-arrow").click(function(event){
          // $('html, body').animate({scrollTop: '+=1050px'}, 800);
          $('html, body').animate({scrollTop: $(document).height()*0.51}, 800);
      });
      $("#down-arrow-mob").click(function(event){
          // $('html, body').animate({scrollTop: '+=1050px'}, 800);
          $('html, body').animate({scrollTop: $(document).height()*0.21}, 800);
      });
  });

  window.setTimeout(function() {
    $('.alert.alert.alert').fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
    });
  }, 2500);
});

}(jQuery));
