class ApplicationMailer < ActionMailer::Base
  default from: "raed@app-itch.com"
  layout 'mailer'
end
