class PasswordResendsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:activation_resend][:email].downcase)
    if user
      if user.activated?
        flash[:warning] = "User with this email address has already been
                          activated. Please login"
        redirect_to login_path
      else
        user.resend_activation_link
        flash[:warning] = "Activation link resent. Please check your email"
        redirect_to root_url
      end
    elsif
      flash.now[:danger] = 'User with this email address does not exist!'
      render 'new'
    end
  end
end
