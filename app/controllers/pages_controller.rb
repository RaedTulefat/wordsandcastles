class PagesController < ApplicationController

  def root
    if current_user.nil?
      redirect_to landing_page_path
    else
      redirect_to main_path
    end
  end

  def main
    @games = current_user.games
  end

  def landing_page
  end

  def about
  end

  def contact
  end
end
