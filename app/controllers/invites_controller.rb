class InvitesController < ApplicationController
  def new
    @invite = Invite.new
  end

  def create
    email = invite_params[:email]
    Invite.create(email: email)
    UserMailer.send_invite(email).deliver_now
  end

  def invite_params
    params.require(:invite).permit(:email)
  end
end
