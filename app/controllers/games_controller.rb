class GamesController < ApplicationController

  def start
    board = generate_game
    new_game = Game.create(user_id: current_user.id, state: board)
    redirect_to game_path(game_id: new_game)
  end

  def skip
    game_id = params[:game_id].to_i
    game = Game.find(game_id)
    # loop through the affected lanes
    board = eval(game.state)
    score = game.score

    # Move letters to left and add letters
    board.each do |lane|
      if lane[1][1].join != ""
        if lane[1][0][1].to_i < 1
          redirect_to gameover_path(score: score) and return
        else
          lane[1][0] = "#0"
        end
      end

      # remove first letter
      lane[1].delete_at(1)

      # add letter to last
      array = ["E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "A",
        "A", "A", "A", "A", "A", "A", "A", "A", "I", "I", "I", "I", "I", "I",
        "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "N", "N", "N",
        "N", "N", "N", "R", "R", "R", "R", "R", "R", "T", "T", "T", "T", "T",
        "T", "L", "L", "L", "L", "S", "S", "S", "S", "U", "U", "U", "U", "D",
        "D", "D", "D", "G", "G", "G", "B", "B", "C", "C", "M", "M", "P", "P",
        "F", "F", "H", "H", "V", "V", "W", "W", "Y", "Y", "K", "J", "X", "Q", "Z"]
      size = rand(1...4)
      lane[1] << array.sample(size)
    end

    game.update(state: board)
    redirect_to game_path(game_id: game_id)
  end

  def gameover
    @games = current_user.games
    @score = params[:score]
  end

  def game
    @game_id = params[:game_id]
    game = Game.find(@game_id)
    @board = eval(game.state)

    @word = ""
    @score = game.score
  end

  def generate_game
    board = {
      #  lane1: ["#1","","F","C","A","E","G","U","N","E"],
      #  lane2: ["#2","Q","E","A","","D","M"],
      #  lane3: ["#3","","J","P","D","H","K","V"],
      #  lane4: ["#4","I","","","","","G","Q","","S","I"],
      #  lane5: ["#5","","","","","I","",""],
      #  lane6: ["#6","O","","V","O","","A","D","Z"]

      lane1: ["#1",[""],[""],[""],[""]],
      lane2: ["#1",[""],[""],[""],[""]],
      lane3: ["#1",[""],[""],[""],[""]],
      # lane4: ["#1",[""],[""],[""],[""]],
      # lane5: ["#1",[""],[""],[""],[""]],
      # lane6: ["#1",[""],[""],[""],[""]]
    }
    array = ["E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "A",
      "A", "A", "A", "A", "A", "A", "A", "A", "I", "I", "I", "I", "I", "I",
      "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "N", "N", "N",
      "N", "N", "N", "R", "R", "R", "R", "R", "R", "T", "T", "T", "T", "T",
      "T", "L", "L", "L", "L", "S", "S", "S", "S", "U", "U", "U", "U", "D",
      "D", "D", "D", "G", "G", "G", "B", "B", "C", "C", "M", "M", "P", "P",
      "F", "F", "H", "H", "V", "V", "W", "W", "Y", "Y", "K", "J", "X", "Q", "Z"]
    board.each do |lane|
      # add letter to last
      size = rand(1...4)
      lane[1] << array.sample(size)
    end

    return board
  end

  def word_check
    game_id = params[:game_id].to_i
    game = Game.find(game_id)
    word = params[:answer].downcase
    match = File.read('words.txt').match(/\b#{word}\b/)
    if match.nil?
      flash[:warning] = "#{word} is not a word"
      redirect_to game_path(game_id: game_id)
    else
      # loop through the affected lanes
      board = eval(game.state)
      reference = params[:reference].split(",")

      reference.reverse.each do |ref|
        # remove letters
          board["lane#{ref[0]}".to_sym][ref[1].to_i][ref[2].to_i] = ""
      end

      # Move letters to left and add letters
      board.each do |lane|
        if lane[1][1].join != ""
          if lane[1][0][1].to_i < 1
            old_score = game.score
            score = calculate_score(word.upcase) + old_score

            game.update(score: score)
            redirect_to gameover_path(score: score) and return
          else
            lane[1][0] = "#0"
          end
        end

        # remove first letter
        lane[1].delete_at(1)

        # add letter to last
        array = ["E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "A",
          "A", "A", "A", "A", "A", "A", "A", "A", "I", "I", "I", "I", "I", "I",
          "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "N", "N", "N",
          "N", "N", "N", "R", "R", "R", "R", "R", "R", "T", "T", "T", "T", "T",
          "T", "L", "L", "L", "L", "S", "S", "S", "S", "U", "U", "U", "U", "D",
          "D", "D", "D", "G", "G", "G", "B", "B", "C", "C", "M", "M", "P", "P",
          "F", "F", "H", "H", "V", "V", "W", "W", "Y", "Y", "K", "J", "X", "Q", "Z"]
        size = rand(1...4)
        lane[1] << array.sample(size)
      end

      # score
      # score = word.split("").count
      old_score = game.score
      score = calculate_score(word.upcase) + old_score

      game.update(state: board, score: score)
      redirect_to game_path(game_id: game_id)
    end
  end

  def calculate_score(word)
    score = 0
    word.split("").each  do |letter|
      if "EAIONRTLSU".include?(letter)
        score += 1
      elsif "DG".include?(letter)
        score += 2
      elsif "BCMP".include?(letter)
        score += 3
      elsif "FHVWY".include?(letter)
        score += 4
      elsif "K"
        score += 5
      elsif "JX".include?(letter)
        score += 8
      else "QZ".include?(letter)
        score += 10
      end
    end

    return score
  end
end
