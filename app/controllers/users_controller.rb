class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]

  def show
    @user = User.find(params[:id])
    # @microposts = @user.microposts.paginate(page: params[:page])
  end

  def index
    # this is here because app crashes when signup form is submitted empty-
    # and refresh button is pressed
    redirect_to root_path
  end

  def new
    @user = User.new
  end

  def edit
  end

  def update
    # checking if email has been updated
    old_email = current_user.email
    new_email = params["user"]["email"]
    # checking if newsletter has been updated
    old_newsletter = current_user.newsletter
    if params["user"]["newsletter"] == "1"
      new_newsletter = true
    else
      new_newsletter = false
    end

    if old_email != new_email
      # new email address
      if old_newsletter == true
        if new_newsletter == false
          # debugger
          # unsubscriber from mailchimp
          newsletter_unsubscribe(old_email)
        else
          # debugger
          # a subscriber => update mailchimp
          newsletter_update(old_email, new_email)
        end
      elsif new_newsletter == true
        # new mailchimp subscription
        newsletter_subscribe(new_email)
      end
    elsif old_newsletter != new_newsletter
      # no change in email address change only in subscription
      if new_newsletter == true
        # debugger
        # new mailchimp subscription
        newsletter_subscribe(old_email)
      else
        # debugger
        # mailchimp unsubscribe
        newsletter_unsubscribe(old_email)
      end
    end


    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated!"
      redirect_to @user unless performed?
    elsif
      render 'edit'
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = 'Please check your email to activate your account'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def newsletter_subscribe(email)
    begin
      # raise "error"
      response = Mailchimp::API.new("mailchimp-api-key-here")
      result = response.lists.subscribe("mailchimp-list-id-here", { "email" => email})
    rescue
      # f383712141 when ever this code is change below address should be changed too!
      newsletter_unsubscribe(email)
      redirect_to "address to where to redirect to incase of failure"
    end
  end

  def newsletter_unsubscribe(email)
    begin
      response = Mailchimp::API.new("mailchimp-api-key-here")
      response.lists.unsubscribe("mailchimp-list-id-here", { "email" => email})
    rescue
      # incase user has "." in the email address and subscribed with out the "."
      # as gmail ignores the "." between firstname and lastname
      # this was not tested properly!
      flash[:info] = '                                                         It appears that email address on file is different from one you subscribed with, Click unsubscribe from the email we previous sent you'
    end
  end

  def newsletter_update(old_email, new_email)
    begin
      response = Mailchimp::API.new("mailchimp-api-key-here")
      subscriber = response.helper.search_members(current_user.email, id = "mailchimp-list-id-here")
      # debugger
      response.lists.update_member("mailchimp-list-id-here",{'email' => old_email},{'email' => new_email})
    rescue
      newsletter_unsubscribe(old_email)
      redirect_to "address to where to redirect to incase of failure"
    end
  end

  private
    # this technique is called strong parameters incase i want to google this later
    def user_params
      params.require(:user).permit(:name,
                                   :email,
                                   :password,
                                   :password_confirmation,
                                   :newsletter)
    end

    #confirms a logged-in user_params
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    #confirms the correct user
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
 end
