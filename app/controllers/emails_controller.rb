class EmailsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def post
    # process various message parameters:
    sender  = params['from']
    subject = params['subject']
    actual_body = params["stripped-text"]

    email = Email.new(sender: sender, subject: subject, body: actual_body)
    if email.save
      render :text => "OK"
    else
      render :text => "ERROR: CHECK THE LOGS"
    end
  end
end
