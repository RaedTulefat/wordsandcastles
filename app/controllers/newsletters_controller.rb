class NewslettersController < ApplicationController
  def new
    @newsletter = Newsletter.new
  end

  def create
    email = newsletter_params[:email]
    Newsletter.create(email: email)
    subscribe_user(email)
  end

  def subscribe_user(email)
    begin
      flash[:danger] = "Woops, This feature does not yet work!"
      redirect_to root_path
      # newsletter = Mailchimp::API.new("88c621f525cd5879567783850e8451b7-us13")
      # newsletter.lists.subscribe("f383712141",{ "email" => email})
      # # current_user.update(mailchimp: true)
      # # flash[:success] = "Success! Thank you for subscribing to our Newsletter. Please check your email and click the confirmation link."
      # redirect_to new_newsletters_path
    rescue
      # current_user.update(mailchimp: true)
      flash[:danger] = "Woops, This feature does not yet work!"
      redirect_to root_path
    end
  end

  # def unsubscribe_user
  #   begin
  #     newsletter = Mailchimp::API.new("88c621f525cd5879567783850e8451b7-us13")
  #     newsletter.lists.unsubscribe("f12239a7d7",
  #                      { "email" => current_user.email})
  #     current_user.update(mailchimp: false)
  #     flash[:success] = "You have been removed from Newsletter subscribed list, so sad"
  #     redirect_to edit_user_registration_path
  #   rescue
  #     current_user.update(mailchimp: false)
  #     redirect_to edit_user_registration_path
  #   end
  # end

  def newsletter_params
    params.require(:newsletter).permit(:email)
  end
end
