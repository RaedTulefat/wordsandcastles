class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or root_path
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def social_create
    user = User.from_omniauth(env["omniauth.auth"])
    if session[:user_id].nil?
      session[:user_id] = User.find_by(email: user.email).id
    else
      session[:user_id] = user.id
    end
    redirect_to root_path
  end

  def google_destroy
    session[:user_id] = nil
    redirect_to root_path
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
