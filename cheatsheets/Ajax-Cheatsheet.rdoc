Step 1: decide what needs to be rendered in ajax
  once decided. put it in an html file (partial) inside the folder with id name e.g _render_table.html.erb

Step 2: add the a div where the above code was with id name
  <div id="render-table">
    <%= render 'resumes/render_table' %>
  </div>

Step 3: Test if it renders correctly

Step 4: inside the partial - Add to the end of the link
  remote: true,
  //this will stop the page from loading! the changes will take effect only if refresh is pressed
  //this is not the desired behaviour we want the page to not load but still displace the changes

Step 2 Add to the end of the action in the controller
  respond_to do |format|
    format.js
  end

Step 3 create file
  name the file same as the action in the controller e.g resume_remove.js.erb

Step 4 add the js code to load the new section of the specific #id
  $('#watchlist_table').html("<%= escape_javascript(render(:partial => 'layouts/watchlist_table')).html_safe %>");
